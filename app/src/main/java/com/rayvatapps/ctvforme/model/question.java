package com.rayvatapps.ctvforme.model;
import java.util.List; /**
 * Created by Mitesh Makwana on 13/08/18.
 */
public class question {
    private int ID;
    private String OPID;
    private String Question;
    private String Active;
    private String Question_order;
    private String[] Options;
    private List<String> OptionsList;

    public question(int ID, String question, String active, String question_order) {
        this.ID = ID;
        Question = question;
        Active = active;
        Question_order = question_order;
    }

    public question(int id, String question, String[] option) {
        this.ID = id;
        Question = question;
        Options=option;
    }

    public question(int id,String opid, String question, List<String> optionList) {
        this.ID = id;
        this.OPID = opid;
        Question = question;
        OptionsList=optionList;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getOPID() {
        return OPID;
    }

    public void setOPID(String OPID) {
        this.OPID = OPID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public String getQuestion_order() {
        return Question_order;
    }

    public void setQuestion_order(String question_order) {
        Question_order = question_order;
    }

    public String[] getOptions() {
        return Options;
    }

    public void setOptions(String[] options) {
        Options = options;
    }

    public List<String> getOptionsList() {
        return OptionsList;
    }

    public void setOptionsList(List<String> optionsList) {
        OptionsList = optionsList;
    }
}
