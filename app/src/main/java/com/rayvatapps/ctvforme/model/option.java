package com.rayvatapps.ctvforme.model;
import java.util.List;

/**
 * Created by Mitesh Makwana on 13/08/18.
 */
public class option {
    private String question;
    private List<String> OptionsList;
    private String[] option;

    public option(String question, List<String> optionList) {
        this.question=question;
        this.OptionsList=optionList;
    }

    public option(String question, String[] option) {
        this.question=question;
        this.option=option;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getOptionsList() {
        return OptionsList;
    }

    public void setOptionsList(List<String> optionsList) {
        OptionsList = optionsList;
    }

    public String[] getOption() {
        return option;
    }

    public void setOption(String[] option) {
        this.option = option;
    }
}
