package com.rayvatapps.ctvforme;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.AlarmClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.karan.churi.PermissionManager.PermissionManager;
import com.rayvatapps.ctvforme.jsonurl.Config;
import com.rayvatapps.ctvforme.model.option;
import com.rayvatapps.ctvforme.model.question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import static java.util.Calendar.MINUTE;

/**
 * Created by Mitesh Makwana on 13/08/18.
 */
public class MainActivity extends AppCompatActivity {
    private Context mContext=MainActivity.this;

    TextView tvQuestion;
    Button btnOp1,btnOp2,btnOp3,btnOp4,btnSubmit;
    LinearLayout lnOp1,lnOp2,lnOp3,lnOp4,lnSubmit;
    LinearLayout lnAnswer,lnMain;

    String HttpUrl = Config.QUESTION_URL;
    private List<option> questionList;
    private List<question> albumList;
    private List<String> optionList;
    int questionno=0;
    ProgressDialog progressDialog;
    List<String> questionForm,answerForm,answerStringForm;

    PermissionManager permissionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        albumList=new ArrayList<>();
        questionForm=new ArrayList<>();
        answerForm=new ArrayList<>();
        answerStringForm=new ArrayList<>();

        loadquestion();

        tvQuestion=(TextView)findViewById(R.id.tvquestion);
//        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        btnOp1=(Button)findViewById(R.id.btnopone);
        btnOp2=(Button)findViewById(R.id.btnoptwo);
        btnOp3=(Button)findViewById(R.id.btnopthree);
        btnOp4=(Button)findViewById(R.id.btnopfour);
//        btnSubmit=(Button)findViewById(R.id.btnnext);
        lnOp1=(LinearLayout) findViewById(R.id.lnopone);
        lnOp2=(LinearLayout) findViewById(R.id.lnoptwo);
        lnOp3=(LinearLayout) findViewById(R.id.lnopthree);
        lnOp4=(LinearLayout) findViewById(R.id.lnopfour);
//        lnSubmit=(LinearLayout) findViewById(R.id.lnsubmit);
        lnAnswer=(LinearLayout) findViewById(R.id.lnanswer);
        lnMain=(LinearLayout) findViewById(R.id.lnmain);

        questionList=new ArrayList<>();
        optionList=new ArrayList<>();

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading..."); // Setting Message
//        progressDialog.setTitle("ProgressDialog"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

       btnOp1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               answerForm.add("1");
               answerStringForm.add(btnOp1.getText().toString().trim());
               manageClick();
           }
       });

       btnOp2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               answerForm.add("2");
               answerStringForm.add(btnOp2.getText().toString().trim());
               manageClick();
           }
       });

       btnOp3.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               answerForm.add("3");
               answerStringForm.add(btnOp3.getText().toString().trim());
               manageClick();
           }
       });

       btnOp4.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               answerForm.add("4");
               answerStringForm.add(btnOp4.getText().toString().trim());
               manageClick();
           }
       });

       getpermissions();
    }
    public String getLocalIpAddress(){
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    private void manageClick() {
        if(questionList.size()>questionno)
        {
            setQuestion();
        }
        else
        {
            lnAnswer.setVisibility(View.VISIBLE);
            lnMain.setVisibility(View.GONE);
            showDialog();

            Thread timer = new Thread(){

                @Override
                public void run() {

                    try {
                        sleep(2000);
                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("question",questionForm.toString());
                        editor.putString("answer",answerForm.toString());
                        editor.commit();
                        Intent a = new Intent(mContext,FormActivity.class);
//                        a.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(a);
                        finish();
                        super.run();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            };

            timer.start();


        }
    }

    private void showDialog() {
      /*  final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_answer);
        dialog.setTitle("Reminder");
        dialog.setCanceledOnTouchOutside(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setAttributes(lp);

//        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();
        */

        TextView tvprimarytitle=(TextView)findViewById(R.id.tvprimarytitle);
        TextView tvprimary=(TextView)findViewById(R.id.tvprimary);
        TextView tvdownloadtitle=(TextView)findViewById(R.id.tvdownloadtitle);
        TextView tvdownload=(TextView)findViewById(R.id.tvdownload);
        TextView tvprovidertitle=(TextView)findViewById(R.id.tvprovidertitle);
        TextView tvprovider=(TextView)findViewById(R.id.tvprovider);

        Animation animEffect = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_effect);

        for (int i=0;i<answerStringForm.size();i++){
            if(i==0)
            {
                tvprimary.setText(answerStringForm.get(i));

                tvprimarytitle.setVisibility(View.VISIBLE);
                tvprimary.setVisibility(View.VISIBLE);
                tvprimarytitle.startAnimation(animEffect);
                tvprimary.startAnimation(animEffect);
            }
            else if(i==1)
            {
                tvdownload.setText(answerStringForm.get(i));

                tvdownloadtitle.setVisibility(View.VISIBLE);
                tvdownload.setVisibility(View.VISIBLE);
                tvdownloadtitle.startAnimation(animEffect);
                tvdownload.startAnimation(animEffect);
            }
            else if(i==2)
            {
                tvprovider.setText(answerStringForm.get(i));

                tvprovidertitle.setVisibility(View.VISIBLE);
                tvprovider.setVisibility(View.VISIBLE);
                tvprovidertitle.startAnimation(animEffect);
                tvprovider.startAnimation(animEffect);
            }
        }

    }

    private void loadquestion() {
        albumList.clear();
//        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
//                        progressBar.setVisibility(View.INVISIBLE);
                        try {
//                            Log.e("Data",response);

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            JSONArray heroArray = obj.getJSONArray("response");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                int id;
                                String opid="";
                                String question,active,question_order;
                                String[] option;
                                //creating a hero object and giving them the values from json object
                                id= heroObject.getInt("ID");
                                questionForm.add(String.valueOf(id));
                                question=heroObject.getString("Question");
                                active=heroObject.getString("Active");
                                question_order=heroObject.getString("Question_order");

                                JSONArray optionArray = heroObject.getJSONArray("Options");
                                optionList.clear();
                                option=new String[optionArray.length()];
                                for (int j = 0; j < optionArray.length(); j++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject optionObject = optionArray.getJSONObject(j);

//                                    String na=optionObject.getString("option");
                                    opid=optionObject.getString("option");
                                    option[j]=optionObject.getString("option");
                                    optionList.add(optionObject.getString("option"));

                                }
                                option option1=new option(question,option);
                                questionList.add(option1);

                            }
                            setQuestion();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        Log.e("error", String.valueOf(volleyError));

//                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void setQuestion() {
        btnOp1.setVisibility(View.GONE);
        btnOp2.setVisibility(View.GONE);
        btnOp3.setVisibility(View.GONE);
        btnOp4.setVisibility(View.GONE);
        option option=questionList.get(questionno);
        tvQuestion.setText(option.getQuestion());
        String[] opt=option.getOption();
        Log.e("data",opt.toString());
        for (int j=0;j<opt.length;j++)
        {
            String text=opt[j];
            if(j==0)
            {
                btnOp1.setText(text);
                btnOp1.setVisibility(View.VISIBLE);
            }
            else if(j==1)
            {
                btnOp2.setText(text);
                btnOp2.setVisibility(View.VISIBLE);
            }
            else if(j==2)
            {
                btnOp3.setText(text);
                btnOp3.setVisibility(View.VISIBLE);
            }
            else if(j==3)
            {
                btnOp4.setText(text);
                btnOp4.setVisibility(View.VISIBLE);
            }
        progressDialog.dismiss();
        }
        questionno+=1;
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public void ifCancelledAndCanRequest(Activity activity) {
                // Do Customized operation if permission is cancelled without checking "Don't ask again"
                // Use super.ifCancelledAndCanRequest(activity); or Don't override this method if not in use
                showToast("Restart app to allow permission");
            }

            @Override
            public void ifCancelledAndCannotRequest(Activity activity) {
                // Do Customized operation if permission is cancelled with checking "Don't ask again"
                // Use super.ifCancelledAndCannotRequest(activity); or Don't override this method if not in use
                showToast("Please grant permission manually");
            }

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(Manifest.permission.INTERNET);
                customPermission.add(Manifest.permission.ACCESS_NETWORK_STATE);
                customPermission.add(Manifest.permission.ACCESS_WIFI_STATE);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(mContext)) {
                // Do stuff here
            }
            else {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
