package com.rayvatapps.ctvforme.jsonurl;
/**
 * Created by Mitesh Makwana on 13/08/18.
 */
public class Config {

    public static final String SERVER_URL="https://www.ctvforme.com/api/api.php?request=";

    public static final String QUESTION_URL= SERVER_URL+"getquestions";
    public static final String ANSWER_URL= SERVER_URL+"add_answer";
}