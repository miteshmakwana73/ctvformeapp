package com.rayvatapps.ctvforme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


public class SplashActivity extends AppCompatActivity {
    private Context mContext=SplashActivity.this;

    Intent intent;
    View mDecorView;
    ImageView imageView;
    RelativeLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        linearLayout=(RelativeLayout) findViewById(R.id.lnsplash);

        ImageView imageView = (ImageView) findViewById(R.id.centerImage);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        imageView.startAnimation(animation);
//        changecolor(a);
        mDecorView = getWindow().getDecorView();

        hideSystemUI();

//        loadanimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    intent = new Intent(mContext,MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                }
            }
        }, 3000);
    }

    private void changecolor(final int b) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if(b==1) {
                        linearLayout.setBackgroundColor(Color.YELLOW);
                        changecolor(b+1);
                    }
                    else {
                        linearLayout.setBackgroundColor(Color.YELLOW);
                        changecolor(b+1);
                    }
                } catch (Exception e) {

                }
            }
        }, 1000);
    }

    private void loadanimation() {
        Thread timer = new Thread(){

            @Override
            public void run() {

                try {
                    sleep(3000);
                    Intent intent = new Intent(mContext,MainActivity.class);
                    startActivity(intent);
                    finish();
                    super.run();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        };

        timer.start();
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
