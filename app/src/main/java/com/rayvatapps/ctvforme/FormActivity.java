package com.rayvatapps.ctvforme;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.ctvforme.jsonurl.Config;

import org.json.JSONObject;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by Mitesh Makwana on 16/08/18.
 */
public class FormActivity extends AppCompatActivity {
    private Context mContext=FormActivity.this;

    private Toolbar toolbar;
    private EditText edName, edEmail, edPhone, edZipcode;
    private TextInputLayout tilName, tilEmail, tilPhone, tilZipcode;
    private Button btnSubmit;
    TextView tvSuccess;
    LinearLayout lnMain;
    String HttpUrl = Config.ANSWER_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        hideKeyboard();
        tilName = (TextInputLayout) findViewById(R.id.tilname);
        tilEmail = (TextInputLayout) findViewById(R.id.tilemail);
        tilPhone = (TextInputLayout) findViewById(R.id.tilphone);
        tilZipcode = (TextInputLayout) findViewById(R.id.tilzipcode);
        edName = (EditText) findViewById(R.id.edname);
        edEmail = (EditText) findViewById(R.id.edemail);
        edPhone = (EditText) findViewById(R.id.edphone);
        edZipcode = (EditText) findViewById(R.id.edzipcode);
        btnSubmit = (Button) findViewById(R.id.btnsubmit);
        tvSuccess=(TextView)findViewById(R.id.tvsuccess);
        lnMain=(LinearLayout)findViewById(R.id.lnmain);

        edName.addTextChangedListener(new MyTextWatcher(edName));
        edEmail.addTextChangedListener(new MyTextWatcher(edEmail));
        edPhone.addTextChangedListener(new MyTextWatcher(tilPhone));
        edZipcode.addTextChangedListener(new MyTextWatcher(tilZipcode));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(mContext);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);    }

    private void submitForm() {

        final String name,email,phone,zipcode;
        if (!validateName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!validatePhone()) {
            return;
        }

        if (!validateZipcode()) {
            return;
        }
        showToast("Thank You");

        name=edName.getText().toString().trim();
        email=edEmail.getText().toString().trim();
        phone=edPhone.getText().toString().trim();
        zipcode=edZipcode.getText().toString().trim();
        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading..."); // Setting Message
//        progressDialog.setTitle("ProgressDialog"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                lnMain.setVisibility(View.GONE);
                                tvSuccess.setVisibility(View.VISIBLE);
                            } else {
                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String question = sharedPreferences.getString("question","");
                String answer = sharedPreferences.getString("answer","");
                String ip=getDeviceIpAddress();
                Log.e("ip",ip);

                question = question.toString().replace("[", "").replace("]", "");
                answer = answer.toString().replace("[", "").replace("]", "");

                // Adding All values to Params.
                params.put("name", name);
                params.put("email", email);
                params.put("phone", phone);
                params.put("zipcode", zipcode);
                params.put("questions", question);
                params.put("answers", answer);
                params.put("ip", ip);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private boolean validateName() {
        if (edName.getText().toString().trim().isEmpty()) {
            tilName.setErrorEnabled(true);
            tilName.setError(getResources().getString(R.string.err_msg_name));
            requestFocus(edName);
            return false;
        } else {
            tilName.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateEmail() {
        String email = edEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            tilEmail.setError(getString(R.string.err_msg_email));
            requestFocus(edEmail);
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePhone() {
        if (edPhone.getText().toString().trim().isEmpty()) {
            tilPhone.setError(getString(R.string.err_msg_phone));
            requestFocus(edPhone);
            return false;
        } else {
            tilPhone.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateZipcode() {
        if (edZipcode.getText().toString().trim().isEmpty()) {
            tilZipcode.setError(getString(R.string.err_msg_zipcode));
            requestFocus(edZipcode);
            return false;
        } else {
            tilZipcode.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String text = edPhone.getText().toString();
            int textlength = edPhone.getText().length();

            Log.e("length", String.valueOf(textlength));
            if (text.endsWith(" "))
                return;

            if (textlength == 1) {
                if (!text.contains("(")) {
                    edPhone.setText(new StringBuilder(text).insert(text.length() - 1, "(").toString());
                    edPhone.setSelection(edPhone.getText().length());
                }
            } else if (textlength == 5) {
                if (!text.contains(")")) {
                    edPhone.setText(new StringBuilder(text).insert(text.length() - 1, ")").toString());
                    edPhone.setSelection(edPhone.getText().length());
                }
            } else if (textlength == 6) {
                if (!text.contains(" ")) {
                    edPhone.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                    edPhone.setSelection(edPhone.getText().length());
                }
            } else if (textlength == 10) {
                if (!text.contains("-")) {
                    edPhone.setText(new StringBuilder(text).insert(text.length() - 1, "-").toString());
                    edPhone.setSelection(edPhone.getText().length());
                }
            }
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edname:
                    validateName();
                    break;
                case R.id.edemail:
                    validateEmail();
                    break;
                case R.id.edphone:
                    validatePhone();
                    break;
                case R.id.edzipcode:
                    validateZipcode();
                    break;
            }
        }
    }

    public String getLocalIpAddress(){
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    @NonNull
    private String getDeviceIpAddress() {
        String actualConnectedToNetwork = null;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager != null) {
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWifi.isConnected()) {
                actualConnectedToNetwork = getWifiIp();
            }
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = getNetworkInterfaceIpAddress();
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = "127.0.0.1";
        }
        return actualConnectedToNetwork;
    }

    @Nullable
    private String getWifiIp() {
        final WifiManager mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (mWifiManager != null && mWifiManager.isWifiEnabled()) {
            int ip = mWifiManager.getConnectionInfo().getIpAddress();
            return (ip & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "."
                    + ((ip >> 24) & 0xFF);
        }
        return null;
    }

    @Nullable
    public String getNetworkInterfaceIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {
                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
